export default [
  {
    path: '/lesson1',
    name: 'Lesson1',
    component: () => import(/* webpackChunkName: "lessons" */ '../../views/lessons/Lesson1.vue'),
  },
  {
    path: '/lesson2',
    name: 'Lesson2',
    component: () => import(/* webpackChunkName: "lessons" */ '../../views/lessons/Lesson2.vue'),
  },
  {
    path: '/lesson3',
    name: 'Lesson3',
    component: () => import(/* webpackChunkName: "lessons" */ '../../views/lessons/Lesson3.vue'),
  },
  {
    path: '/lesson4',
    name: 'Lesson4',
    component: () => import(/* webpackChunkName: "lessons" */ '../../views/lessons/Lesson4.vue'),
  },
];
