import { createRouter, createWebHistory } from 'vue-router';
import Root from '../views/Root.vue';
import Lessons from './modules/lessons';
import Workshop from '../views/Workshop.vue';

const routes = [
  {
    path: '/',
    name: 'Root',
    component: Root,
    children: [
      ...Lessons,
    ],
  },
  {
    path: '/workshop',
    name: 'Workshop',
    component: Workshop,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
