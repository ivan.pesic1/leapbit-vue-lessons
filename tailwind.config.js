const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    './public/index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      red: colors.red,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      slate: colors.slate,
      lessons: '#42b983',
    },
    fontFamily: {
      raleway: ['raleway', 'sans-serif'],
      barlow: ['barlow', 'sans-serif'],
    },
    extend: {},
  },
  plugins: [],
};
